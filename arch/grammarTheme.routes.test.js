const mongoose = require('mongoose');
const GrammarTheme = require('../src/db/models/practice/grammarTheme.model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../src/index');
let should = chai.should();
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');

chai.use(chaiHttp);

describe("GrammarThemes", () => {

    describe('GET', () => {
        before(async () => {
            await GrammarTheme.deleteMany({});
        });

        it('it should get all themes', (done) => {
            chai.request(server)
                .get('/api/grammar/theme')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

    });

    // describe('POST', () => {
    //     let token;
    //     before(async (done) => {
    //         console.log(await chai.request(server)
    //             .post('/api/login')
    //             .send({
    //                 login: "testLogin2",
    //                 password: "testPassword"
    //             })
    //             .end((err, res) => {
    //                 if (!err && res.body.token) {
    //                     token = res.body.token;
    //                 }
    //             })
    //         )
    //         done();
    //     });
    //
    //     it('it should create new theme', (done) => {
    //         console.log("TOKEN === >>>", token);
    //         // chai.request(server)
    //         //     .post('/api/grammar/theme')
    //         done();
    //     })
    // })


    // it('bad auth data', (done) => {
    //     const testUser = {
    //         login: "arch",
    //         password: "arch",
    //         firstName: "TestFirstName",
    //         lastName: "TestLastName",
    //         role: "teacher"
    //     };
    //     chai.request(server)
    //         .post('/api/register')
    //         .send(testUser)
    //         .end((err, res) => {
    //             res.should.have.status(400);
    //             res.body.should.be.a('object');
    //             res.body.should.have.property('errors');
    //             res.body.should.have.property('message').eq('Validation Error!');
    //             done();
    //         });
    //
    // });

    // it('user already regist.', (done) => {
    //     const testUser = {
    //         login: "testLogin2",
    //         password: "testPassword",
    //         firstName: "TestFirstName",
    //         lastName: "TestLastName",
    //         role: "teacher"
    //     };
    //     chai.request(server)
    //         .post('/api/register')
    //         .send(testUser)
    //         .end((err, res) => {
    //             res.should.have.status(400);
    //             res.body.should.be.a('object');
    //             res.body.should.have.property('message').eq('User is already registered');
    //             done();
    //         });
    //
    // });

    // it('bad data for saving', (done) => {
    //     const testUser = {
    //         login: "testLogin0",
    //         password: "testPassword",
    //         firstName: "TestFirstName",
    //         lastName: "TestLastName"
    //     };
    //     chai.request(server)
    //         .post('/api/register')
    //         .send(testUser)
    //         .end((err, res) => {
    //             res.should.have.status(400);
    //             res.body.should.be.a('object');
    //             res.body.should.have.property('message').eq('Wrong data!');
    //             done();
    //         });
    //
    // });


    // describe('login', () => {
    //
    //     it('success login', (done) => {
    //         const testUser = {
    //             login: "testLogin2",
    //             password: "testPassword"
    //         };
    //         chai.request(server)
    //             .post('/api/login')
    //             .send(testUser)
    //             .end((err, res) => {
    //                 res.should.have.status(200);
    //                 res.body.should.be.a('object');
    //                 res.body.should.have.property('token');
    //                 res.body.should.have.property('role');
    //                 res.body.should.have.property('firstName');
    //                 res.body.should.have.property('lastName');
    //                 done();
    //             });
    //     });
    //
    //     it('incorrect password', (done) => {
    //         const testUser = {
    //             login: "testLogin2",
    //             password: "IncorrectPassword"
    //         };
    //         chai.request(server)
    //             .post('/api/login')
    //             .send(testUser)
    //             .end((err, res) => {
    //                 res.should.have.status(403);
    //                 res.body.should.be.a('object');
    //                 res.body.should.have.property('message').eq('Wrong user data!');
    //                 done();
    //             });
    //     });
    //
    //     it('incorrect data', (done) => {
    //         const testUser = {
    //             login: "testL",
    //             password: "testP"
    //         };
    //         chai.request(server)
    //             .post('/api/login')
    //             .send(testUser)
    //             .end((err, res) => {
    //                 res.should.have.status(403);
    //                 res.body.should.be.a('object');
    //                 res.body.should.have.property('message').eq('Wrong user data!');
    //                 done();
    //             });
    //     });
    // });

});
