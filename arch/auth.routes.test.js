const User = require('../src/db/models/user.model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../src/index');
let should = chai.should();

chai.use(chaiHttp);

describe("Authorization", () => {

    describe('register', () => {
        before(async () => {
            await User.deleteMany({});
        });

        it('it should create new user', (done) => {
            const testUser = {
                login: "testLogin2",
                password: "testPassword",
                firstName: "TestFirstName",
                lastName: "TestLastName",
                role: "teacher"
            };
            chai.request(server)
                .post('/api/register')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('role');
                    done();
                });

        });

        it('bad auth data', (done) => {
            const testUser = {
                login: "test",
                password: "test",
                firstName: "TestFirstName",
                lastName: "TestLastName",
                role: "teacher"
            };
            chai.request(server)
                .post('/api/register')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.should.have.property('message').eq('Validation Error!');
                    done();
                });

        });

        it('user already regist.', (done) => {
            const testUser = {
                login: "testLogin2",
                password: "testPassword",
                firstName: "TestFirstName",
                lastName: "TestLastName",
                role: "teacher"
            };
            chai.request(server)
                .post('/api/register')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('User is already registered');
                    done();
                });

        });

        it('bad data for saving', (done) => {
            const testUser = {
                login: "testLogin0",
                password: "testPassword",
                firstName: "TestFirstName",
                lastName: "TestLastName"
            };
            chai.request(server)
                .post('/api/register')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('Wrong data!');
                    done();
                });

        });

    });

    describe('login', () => {

        it('success login', (done) => {
            const testUser = {
                login: "testLogin2",
                password: "testPassword"
            };
            chai.request(server)
                .post('/api/login')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('role');
                    res.body.should.have.property('firstName');
                    res.body.should.have.property('lastName');
                    done();
                });
        });

        it('incorrect password', (done) => {
            const testUser = {
                login: "testLogin2",
                password: "IncorrectPassword"
            };
            chai.request(server)
                .post('/api/login')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('Wrong user data!');
                    done();
                });
        });

        it('incorrect data', (done) => {
            const testUser = {
                login: "testL",
                password: "testP"
            };
            chai.request(server)
                .post('/api/login')
                .send(testUser)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('Wrong user data!');
                    done();
                });
        });
    });

});
