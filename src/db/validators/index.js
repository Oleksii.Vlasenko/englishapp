module.exports.isValidName = (name) => {
    return name.match(/^\p{Lu}/u) && (name.match(/\p{Ll}/gu).length === (name.length - 1));
};

module.exports.isValidPassword = (password) => {
    return password.match(/\p{Lu}/u) && password.match(/\p{Ll}/u) && password.match(/\p{Nd}/u);
};

module.exports.isLowerCaseWord = (word) => {
    return word.match(/\p{Ll}/gu).length === word.length;
};

module.exports.isOnlyNumbers = (number) => {
    return Number.isInteger(number);
};
