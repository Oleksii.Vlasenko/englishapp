const mongoose = require('mongoose');

const taskGroupSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String
    },
    expDate: {
        type: Date
    },
    description: {
        type: String
    },
    teacher: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    groups: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Group"
        }
    ],
    difficulty: {
        type: String
    },
    taskType: {
        type: String
    },
    time: {
        type: Number
    },
    active: {
        type: Boolean
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('TaskGroup', taskGroupSchema);
