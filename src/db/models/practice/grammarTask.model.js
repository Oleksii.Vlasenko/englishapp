const mongoose = require('mongoose');

const grammarTaskSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstPart: String,
    lastPart: String,
    answer: String,
    isPublic: Boolean,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    taskGroups: [
        {
            type: mongoose.Schema.Types.ObjectId
        }
    ],
    options: [
        {
            type: String,
            require: true
        }
    ],
    translation: {
        type: String,
        require: true
    },
    infinitive: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('GrammarTask', grammarTaskSchema);
