const mongoose = require('mongoose');
const Validator = require('../../validators/index');

const vocabularyTaskSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    word: String,
    transcription: String,
    translation: String,
    description: String,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    taskGroups: [
        {
            type: mongoose.Schema.Types.ObjectId
        }
    ],
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('VocabularyTask', vocabularyTaskSchema);
