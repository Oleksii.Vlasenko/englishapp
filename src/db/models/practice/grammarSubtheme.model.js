const mongoose = require('mongoose');

const grammarSubthemeSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    translation: {
        type: String,
        required: true
    },
    theme: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'GrammarTheme'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('GrammarSubtheme', grammarSubthemeSchema);
