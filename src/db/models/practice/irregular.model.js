const mongoose = require('mongoose');
const Validator = require('../../validators/index')

const irregularSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    infinitive: {
        type: String,
        required: true
    },
    pastSimple: {
        type: String,
        required: true
    },
    pastParticiple: {
        type: String,
        required: true
    },
    translation: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Irregular', irregularSchema);
