const mongoose = require('mongoose');

const markSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    taskGroup: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TaskGroup',
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    value: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Mark', markSchema);
