const express = require('express');
const appLoader = require('./loaders/index');
const config = require('config');
const cors = require('cors');

const app = express();
app.use(cors());

const startServer = (async () => {
    try {
        const PORT = process.env.PORT || config.get('port');
        await appLoader({expressApp: app});

        app.use(cors());

        app.listen(PORT, (err) => {
            if (err) {
                console.log('Something bad happened!');
            }
            console.log('App has started successfully on port ', PORT);
        });
        return app;
    } catch (e) {
        process.exit(1);
    }
})();

module.exports = app;
