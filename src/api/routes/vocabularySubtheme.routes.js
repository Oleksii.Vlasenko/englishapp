const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {
    getVocabularySubthemes,
    createVocabularySubtheme,
    editVocabularySubtheme,
    removeVocabularySubtheme
} = require('../../controllers/vocabularySubtheme.controller');

const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {
    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/vocabulary/subtheme', getVocabularySubthemes);

    router.post('/vocabulary/subtheme', tokenVerify, createVocabularySubtheme);

    router.put('/vocabulary/subtheme/:id?', tokenVerify, editVocabularySubtheme);

    router.delete('/vocabulary/subtheme/:id?', tokenVerify, removeVocabularySubtheme);
};
