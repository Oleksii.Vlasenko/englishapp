const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const tokenVerify = require('../middlewares/tokenVerify');
const {
    getTaskGroups,
    getTaskGroupByStudentId,
    createTaskGroup,
    editTaskGroup,
    removeTaskGroups
} = require('../../controllers/taskGroup.controller');

module.exports = (router) => {

    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/taskgroup', tokenVerify, getTaskGroups);

    router.get('/taskgroup/:id', getTaskGroupByStudentId);

    router.post('/taskgroup', tokenVerify, createTaskGroup);

    router.put('/taskgroup/:id?', tokenVerify, editTaskGroup);

    router.delete('/taskgroup/:id?', tokenVerify, removeTaskGroups);
};
