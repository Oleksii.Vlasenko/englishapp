const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const User = require('../../db/models/user.model');
const tokenVerify = require('../middlewares/tokenVerify');
const {
    getUsers,
    getUserById,
    editUserById,
    removeUserById
} = require('../../controllers/user.controller');

module.exports = (router) => {
    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/user', tokenVerify, getUsers);

    router.get('/user/:id', getUserById);

    router.put('/user/:id', editUserById);

    router.delete('/user/:id', tokenVerify, removeUserById);

};
