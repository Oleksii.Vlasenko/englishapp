const bodyParser = require('body-parser');
const {
    getMarksByUserId,
    getMarksByTaskGroupId,
    createMark,
    editMarkById,
    removeMarkById
} = require('../../controllers/mark.controller');
const tokenVerify = require('../middlewares/tokenVerify');


module.exports = (router) => {

    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/mark', getMarksByUserId);

    router.get('/mark/:id?', tokenVerify, getMarksByTaskGroupId);

    router.post('/mark', createMark);

    router.put('/mark/:id?', editMarkById);

    router.delete('/mark/:id?', tokenVerify, removeMarkById);
};
