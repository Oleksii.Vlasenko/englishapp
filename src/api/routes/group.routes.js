const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const Group = require('../../db/models/group.model');
const {
    getGroups,
    createGroup,
    editGroup,
    removeGroup,
    getGroupStudent
} = require('../../controllers/group.controller');
const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {

    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/group', tokenVerify, getGroups);

    router.get('/group/:id?', tokenVerify, getGroupStudent);

    router.post('/group', tokenVerify, createGroup);

    router.put('/group/:id?', tokenVerify, editGroup);

    router.delete('/group/:id?', tokenVerify, removeGroup);
};
