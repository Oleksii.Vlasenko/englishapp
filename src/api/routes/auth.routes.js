const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');
const User = require('../../db/models/user.model');
const Group = require('../../db/models/group.model');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = (router) => {
    router.use(bodyParser.urlencoded({extended: true}));

    router.post('/register',
        [
            check('login', 'Wrong login').isLength({min: 6}),
            check('password', 'Wrong password').isLength({min: 8}),
        ],
        async (req, res) => {
            try {
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res
                        .status(400)
                        .json({
                            errors: errors.array(),
                            message: "Validation Error!"
                        });
                }
                const {login, password, firstName, lastName, role, email, groupKey} = req.body;
                const group = await Group.findOne({groupKey});
                if (!group && role === "student") {
                    return res.status(400).json({message: "Wrong group key"})
                }
                let groups = group ? [group._id] : [];
                const userToSave = await User.findOne({login});
                if (userToSave) {
                    return res.status(400).json({message: 'User is already registered'});
                }
                const hashedPassword = await bcrypt.hash(password, 12);
                const active = (role === "student");
                const user = new User({
                    _id: new mongoose.Types.ObjectId,
                    firstName,
                    lastName,
                    login,
                    password: hashedPassword,
                    role,
                    email,
                    groups,
                    active
                });
                await user.save((err, regUser) => {
                    if (err) {
                        return res.status(400).json({message: "Wrong data!", error: err});
                    }
                    const token = jwt.sign(
                        {userId: user.id, role},
                        config.get('jwtSecret'),
                        {expiresIn: '1h'}
                    );
                    res.status(201).json({token, role, firstName, lastName, id: regUser._id});
                });
            } catch (e) {
                res.status(500).json({message: 'Something happened!'});
                console.error('Error --> ', e.message);
            }
        });

    router.post('/login', async (req, res) => {
        try {
            const {login, password} = req.body;
            await User.findOne({login}).exec(async (err, user) => {
                if (!user || err) {
                    return res.status(403).json({message: 'Wrong user data!'});
                }
                const isMatch = await bcrypt.compare(password, user.password);
                if (!isMatch) {
                    return res.status(403).json({message: 'Wrong user data!'});
                }
                if (!user.active) {
                    res.status(400).json({message: "Your account has not been verified."})
                }
                const token = jwt.sign(
                    {userId: user.id, role: user.role},
                    config.get('jwtSecret'),
                    {expiresIn: '2 days'}
                );
                res.status(200).json({
                    token,
                    role: user.role,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    id: user._id
                });
            });
        } catch (e) {
            res.status(500).json({message: 'Something happened!'});
        }
    });
};
