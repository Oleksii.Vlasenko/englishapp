const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {
    getGrammarTasks,
    getGrammarTaskBySubtheme,
    createGrammarTask,
    editGrammarTask,
    removeGrammarTask
} = require('../../controllers/grammarTask.controller');

const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {

    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/grammar/task', tokenVerify, getGrammarTasks);

    router.get('/grammar/task/:id', getGrammarTaskBySubtheme);

    router.post('/grammar/task', tokenVerify, createGrammarTask);

    router.put('/grammar/task/:id?', tokenVerify, editGrammarTask);

    router.delete('/grammar/task/:id?', tokenVerify, removeGrammarTask);
};
