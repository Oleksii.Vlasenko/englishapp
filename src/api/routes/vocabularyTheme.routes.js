const mongoose = require('mongoose');
const GrammarTheme = require('../../db/models/practice/grammarTheme.model');
const {
    getVocabularyThemes,
    createVocabularyTheme,
    editVocabularyTheme,
    removeVocabularyTheme
} = require('../../controllers/vocabularyTheme.controller');
const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {

    router.get('/vocabulary/theme', getVocabularyThemes);

    router.post('/vocabulary/theme', tokenVerify, createVocabularyTheme);

    router.put('/vocabulary/theme/:id', tokenVerify, editVocabularyTheme);

    router.delete('/vocabulary/theme/:id', tokenVerify, removeVocabularyTheme);
};
