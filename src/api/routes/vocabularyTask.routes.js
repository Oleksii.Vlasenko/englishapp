const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {
    getVocabularyTasks,
    getVocabularyTaskBySubtheme,
    createVocabularyTask,
    editVocabularyTask,
    removeVocabularyTask
} = require('../../controllers/vocabularyTask.controller');

const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {

    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/vocabulary/task', tokenVerify, getVocabularyTasks);

    router.get('/vocabulary/task/:id', getVocabularyTaskBySubtheme);

    router.post('/vocabulary/task', tokenVerify, createVocabularyTask);

    router.put('/vocabulary/task/:id?', tokenVerify, editVocabularyTask);

    router.delete('/vocabulary/task/:id?', tokenVerify, removeVocabularyTask);
};
