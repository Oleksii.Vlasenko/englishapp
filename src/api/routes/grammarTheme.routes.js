const mongoose = require('mongoose');
const GrammarTheme = require('../../db/models/practice/grammarTheme.model');
const {
    getGrammarThemes,
    createGrammarTheme,
    editGrammarTheme,
    removeGrammarTheme
} = require('../../controllers/grammarTheme.controler');
const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {

    router.get('/grammar/theme', getGrammarThemes);

    router.post('/grammar/theme', tokenVerify, createGrammarTheme);

    router.put('/grammar/theme/:id', tokenVerify, editGrammarTheme);

    router.delete('/grammar/theme/:id', tokenVerify, removeGrammarTheme);
};
