const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {
    getGrammarSubthemes,
    createGrammarSubtheme,
    editGrammarSubtheme,
    removeGrammarSubtheme
} = require('../../controllers/grammarSubtheme.controller');

const tokenVerify = require('../middlewares/tokenVerify');

module.exports = (router) => {
    router.use(bodyParser.urlencoded({extended: true}));

    router.get('/grammar/subtheme', getGrammarSubthemes);

    router.post('/grammar/subtheme', tokenVerify, createGrammarSubtheme);

    router.put('/grammar/subtheme/:id?', tokenVerify, editGrammarSubtheme);

    router.delete('/grammar/subtheme/:id?', tokenVerify, removeGrammarSubtheme);
};
