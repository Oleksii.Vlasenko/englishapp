const {Router} = require('express');
const userRoute = require('./routes/user.routes');
const authRoute = require('./routes/auth.routes');
const groupRoute = require('./routes/group.routes');
const grammarTheme = require('./routes/grammarTheme.routes');
const grammarSubtheme = require('./routes/grammarSubtheme.routes');
const grammarTask = require('./routes/grammarTask.routes');
const taskGroup = require('./routes/taskGroup.routes');
const vocabularyTask = require('./routes/vocabularyTask.routes');
const mark = require('./routes/mark.routes');
const vocabularyTheme = require('./routes/vocabularyTheme.routes');
const vocabularySubtheme = require('./routes/vocabularySubtheme.routes');
const bodyParser = require('body-parser');

module.exports = () => {
    const router = Router();
    router.use(bodyParser.urlencoded({extended: true}));

    userRoute(router);
    authRoute(router);
    groupRoute(router);
    grammarTheme(router);
    grammarSubtheme(router);
    grammarTask(router);
    taskGroup(router);
    vocabularyTask(router);
    mark(router);
    vocabularyTheme(router);
    vocabularySubtheme(router);
    return router;
};
