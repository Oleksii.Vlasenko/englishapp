const jwt = require('jsonwebtoken');
const config = require('config');

const tokenVerify = (req, res, next) => {
    const token = req.headers['authorization'];
    jwt.verify(token, config.get('jwtSecret'), async (err, authData) => {
        if (err) {
            return res.status(403).json({message: "You don`t have enough rights!"});
        }
        if (authData.role !== "teacher" && authData.role !== "admin") {
            return res.status(403).json({message: "You don`t have enough rights!"});
        }
        req.user = {
            userId: authData.userId,
            role: authData.role
        };
        next();
    });
};

module.exports = tokenVerify;
