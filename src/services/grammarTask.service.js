exports.getStandardTasks = (tasks) => {
    let standardTasks = [];
    tasks.forEach((item) => {
        if (item.groups.includes("standard")) {
            standardTasks.push(item);
        }
    });
    return standardTasks;
};
