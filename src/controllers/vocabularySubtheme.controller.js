const mongoose = require('mongoose');
const VocabularySubtheme = require('../db/models/practice/vocabularySubtheme.model');

exports.getVocabularySubthemes = async (req, res) => {
    try {
        await VocabularySubtheme.find({}).exec((err, themes) => {
            if (err) {
                res.status(400).json({message: "Something happend!"});
            }
            res.status(200).json(themes);
        });
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.createVocabularySubtheme = async (req, res) => {
    try {
        const user = req.user.userId;
        const {title, translation, theme} = req.body;
        const subtheme = await VocabularySubtheme.findOne({title, theme});
        if (subtheme) {
            return res.status(400).json({message: "Subtheme have already registered!"});
        }
        const newVocabularySubtheme = new VocabularySubtheme({
            _id: new mongoose.Types.ObjectId,
            title,
            translation,
            theme,
            user,
            created: Date.now(),
            updated: Date.now()
        });
        await newVocabularySubtheme.save((err, theme) => {
            if (err) {
                return res.status(500).json({message: "Something happened. Try again!"});
            }
            res.status(201).json({message: 'Group successfully saved'});
        });
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.editVocabularySubtheme = async (req, res) => {
    try {
        await VocabularySubtheme.findByIdAndUpdate(req.params.id, req.body, (err) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json({message: "Update successfully!"});
        });

    } catch (e) {
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeVocabularySubtheme = async (req, res) => {
    try {
        await VocabularySubtheme.findByIdAndRemove(req.params.id).exec((err) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json({message: "Delete successfully!"});
        });
    } catch (e) {
        res.status(500).json({message: 'Error!'});
    }
};
