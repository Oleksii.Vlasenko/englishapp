const mongoose = require('mongoose');
const Mark = require('../db/models/mark.model');
const jwt = require('jsonwebtoken');
const config = require('config');

exports.getMarksByUserId = async (req, res) => {
    try {
        const token = req.headers['authorization'];
        jwt.verify(token, config.get('jwtSecret'), async (err, authData) => {
            if (err) {
                return res.status(403).json({message: "You don`t have enough rights!"});
            }
            await Mark.find({user: authData.userId}).exec((err, data) => {
                if (err) {
                    return res.status(400).json({message: "Finding error!"});
                }
                res.status(200).json(data);
            })
        })
    } catch (e) {
        res.status(500).json({message: "Something happend!"});
    }
};

exports.getMarksByTaskGroupId = async (req, res) => {
    try {
        await Mark.find({taskGroup: req.params.id}).exec((err, data) => {
            if (err) {
                return res.status(400).json({message: "Finding error!"});
            }
            res.status(200).json(data);
        });
    } catch (e) {
        res.status(500).json({message: "Something happend!"});
    }
};

exports.createMark = async (req, res) => {
    try {
        const token = req.headers['authorization'];
        jwt.verify(token, config.get('jwtSecret'), async (err, authData) => {
            if (err) {
                return res.status(403).json({message: "You don`t have enough rights!"});
            }
            const {taskGroup, value} = req.body;
            const data = await Mark.find({user: authData.userId, taskGroup});
            if (data.length > 0) {
                return res.status(400).json({message: "Mark already created!"});
            }
            const newMark = new Mark({
                _id: new mongoose.Types.ObjectId,
                user: authData.userId,
                value,
                taskGroup,
                created: Date.now(),
                updated: Date.now()
            });
            await newMark.save((err, mark) => {
                if (err) {
                    return res.status(400).json({message: "Saving error!"});
                }
                res.status(201).json(mark);
            })
        })
    } catch (e) {
        res.status(500).json({message: "Something happend!"});
    }
};

exports.editMarkById = async (req, res) => {
    try {
        await Mark.findOne({_id: req.params.id}).exec(async (err, mark) => {
            if (err) {
                return res.status(400).json({message: "Finding error!"});
            }
            if (mark.created - mark.updated === 0) {
                await Mark.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, mark) => {
                    if (err) {
                        return res.status(400).json({message: "Wrong updating data"});
                    }
                    return res.status(200).json(mark);
                });
            } else {
                res.status(400).json({message: "Mark has already saved!", created: `${mark.created - mark.updated}`});
            }
        });
    } catch (e) {
        res.status(500).json({message: "Something happend!"});
    }
};

exports.removeMarkById = async (req, res) => {
    try {
        await Mark.findByIdAndRemove(req.params.id).exec((err, mark) => {
            if (err) {
                return res.status(400).json({message: "Removing error!"});
            }
            res.status(200).json(mark);
        })
    } catch (e) {
        res.status(500).json({message: "Something happend!"});
    }
};
