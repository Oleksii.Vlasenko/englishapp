const mongoose = require('mongoose');
const Group = require('../db/models/group.model');
const User = require('../db/models/user.model');

exports.getGroups = async (req, res) => {
    try {
        let groups = [];
        switch (req.user.role) {
            case "admin":
                groups = await Group.find({});
                break;
            case "teacher":
                groups = await Group.find({teacher: req.user.userId});
                break;
        }
        if (!groups) {
            return res.status(400).json({message: "You don`t have any groups"});
        }
        res.status(200).json([...groups]);
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.getGroupStudent = async (req, res) => {
    try {
        const students = await User.find({groups: req.params.id, role: "student"});
        if (!students) {
            return res.status(400).json({message: "Group is empty"});
        }
        res.status(200).json(students);
    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend!"});
    }
};

exports.createGroup = async (req, res) => {
    try {
        const teacher = req.user.userId;
        const {title, groupKey} = req.body;
        const data = await Group.findOne({groupKey});
        if (data) {
            return res.status(400).json({message: "Not unique group key! Please, try again!"})
        }
        const newGroup = new Group({
            _id: new mongoose.Types.ObjectId,
            title,
            teacher,
            groupKey,
            created: Date.now(),
            updated: Date.now()
        });
        await newGroup.save((err, group) => {
            if (err) {
                return res.status(400).json({message: "Saving error"});
            }
            res.status(201).json(group);
        });

    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happened. Try again!", error: e});
    }
};

exports.editGroup = async (req, res) => {
    try {
        const data = await Group.find({groupKey: req.body.groupKey});
        if (data && data.length > 1) {
            return res.status(400).json({message: "Not unique group key! Please, try again!"});
        }
        await Group.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, item) => {
            if (err) {
                return res.status(400).json({message: "Wrong updating data"});
            }
            res.status(200).json(item);
        });

    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeGroup = async (req, res) => {
    try {
        const data = await User.findOne({groups: req.params.id, role: "student"});
        if (data) {
            return res.status(200).json({message: "Group is not empty!"});
        }
        await Group.findByIdAndRemove(req.params.id).exec((err, group) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(group);
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Error!'});
    }
};
