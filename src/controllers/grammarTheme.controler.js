const mongoose = require('mongoose');
const GrammarTheme = require('../db/models/practice/grammarTheme.model');

exports.getGrammarThemes = async (req, res) => {
    try {
        await GrammarTheme.find({}).exec((err, theme) => {
            if (err) {
                return res.status(400).json({message: "Something happend!"})
            }
            res.status(200).json([...theme]);
        })
    } catch (e) {
        res.status(500).json({message: "Something happend!"})
    }
};

exports.createGrammarTheme = async (req, res) => {
    try {
        const user = req.user.userId;
        const {title, translation} = req.body;
        const theme = await GrammarTheme.findOne({title});
        if (theme) {
            return res.status(400).json({message: "Theme already in use!"})
        }

        const newTheme = new GrammarTheme({
            _id: new mongoose.Types.ObjectId,
            title,
            translation,
            user,
            created: Date.now(),
            updated: Date.now()
        });
        await newTheme.save((err) => {
            if (err) {
                return res.status(400).json({message: "Saving error!"});
            }
            res.status(200).json({message: 'Theme successfully saved!'});
        });
    } catch (e) {
        res.status(500).json({message: 'Something happened!'});
    }
};

exports.editGrammarTheme = async (req, res) => {
    try {
        await GrammarTheme.findOneAndUpdate({_id: req.params.id}, req.body);
        res.status(200).json({message: "Update successfully!"});
    } catch (e) {
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeGrammarTheme = async (req, res) => {
    try {
        await GrammarTheme.findByIdAndRemove(req.params.id).exec((err, theme) => {
            res.json(theme);
        });
    } catch (e) {
        res.status(500).json({message: 'Error!'});
    }
};
