const mongoose = require('mongoose');
const GrammarTask = require('../db/models/practice/grammarTask.model');

exports.getGrammarTasks = async (req, res) => {
    try {
        let publicTasks = [];
        await GrammarTask.find({isPublic: true}).exec( async(err, tasks) => {
            if (err) {
                return res.status(400).json({message: "Something happend!"});
            }
            publicTasks = [...tasks];
            await GrammarTask.find({user: req.user.userId, isPublic: false}).exec((err, tasks) => {
                if (err) {
                    return res.status(400).json({message: "Something happend!"});
                }
                res.status(200).json([...publicTasks, ...tasks]);
            })
        });
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.getGrammarTaskBySubtheme = async (req, res) => {
    try {
            await GrammarTask.find({taskGroups: req.params.id}).exec((err, tasks) => {
                if (err) {
                    return res.status(400).json({message: "Finding error!"});
                }
                res.status(200).json(tasks);
            });

    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.createGrammarTask = async (req, res) => {
    try {
        const user = req.user.userId;
        const {firstPart, lastPart, answer, isPublic, taskGroups, options, translation, infinitive} = req.body;
        const newGrammarTask = new GrammarTask({
            _id: new mongoose.Types.ObjectId,
            user,
            firstPart,
            lastPart,
            answer,
            isPublic,
            taskGroups,
            options,
            translation,
            infinitive,
            created: Date.now(),
            updated: Date.now()
        });
        await newGrammarTask.save((err, task) => {
            if (err) {
                return res.status(500).json({message: "Something happened. Try again!"});
            }
            res.status(201).json(task);
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({...e, message: "Something happened. Try again!"});
    }
};

exports.editGrammarTask = async (req, res) => {
    try {
        await GrammarTask.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, task) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(task);
        });

    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeGrammarTask = async (req, res) => {
    try {
        await GrammarTask.findByIdAndRemove(req.params.id).exec((err, task) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(task);
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Error!'});
    }
};
