const mongoose = require('mongoose');
const User = require('../db/models/user.model');
const Group = require('../db/models/group.model');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcryptjs');

exports.getUsers = async (req, res) => {
    try {
        if (req.user.role === 'teacher') {
            const groups = await Group.find({teacher: req.user.userId});
            const groupList = groups.map(item => `${item._id}`);
            const students = await User.find({role: "student"});
            const result = students.filter(student => groupList.includes(student.groups[0]));
            return res.status(200).json(result);
        }
        if (req.user.role === 'admin') {
            const regexp = req.headers.searchstring
                ? new RegExp(`^` + req.headers.searchstring, `gi`)
                : undefined;
            let {activepage, pagelength,  sortby, role, direction} = req.headers;
            let searchObj = {};
            searchObj = regexp
                ? {...searchObj, lastName: regexp}
                : searchObj;
            await User.find(searchObj).exec((err, users) => {
                if (err) {
                    return res.status(400).json({message: "Finding error!"})
                }
                let result = users;
                if (role) {
                    let roleArr = [];
                    if (role === "7") {
                        roleArr = ["admin", "student", "teacher"];
                    } else {
                        if (role % 2 === 1) roleArr.push("teacher");
                        if (role >= 4) roleArr.push("admin");
                        if (role === "2" || role === "3" || role === "6") roleArr.push("student");
                    }
                    result = users.filter(item => roleArr.includes(item.role));
                }
                result.sort((a, b) => {
                    if (direction > 0) {
                        if (a[sortby] < b[sortby]) {
                            return -1;
                        }
                        if (a[sortby] > b[sortby]) {
                            return 1;
                        }
                        return 0;
                    } else {
                        if (a[sortby] > b[sortby]) {
                            return -1;
                        }
                        if (a[sortby] < b[sortby]) {
                            return 1;
                        }
                        return 0;
                    }
                });
                const pagesLength = result.length;
                const start = (+activepage - 1) * +pagelength;
                const end = +activepage * +pagelength;
                return res
                    .status(200)
                    .json({pagesLength, users: result.slice(start, end)});
            })
        }
    } catch (e) {
        res.status(500).json({message: "Something happened"});
    }
};

exports.getUserById = async (req, res) => {
    try {
        await User.findOne({_id: req.params.id}).exec((err, user) => {
            if (err) {
                return res.status(400).json({message: "Something happened"});
            }
            res.status(200).json(user);
        })
    } catch (e) {
        res.status(500).json({message: "Something happened"});
    }
};

exports.editUserById = async (req, res) => {
    try {
        const token = req.headers['authorization'];
        await jwt.verify(token, config.get('jwtSecret'), async (err, authData) => {
            if (err) {
                return res.status(403).json({message: "You don`t have enough rights!"});
            }
            if (authData.role === "teacher" || authData.role === "admin") {
                let body = {...req.body};
                if (req.body.password) {
                    const password = await bcrypt.hash(req.body.password, 12);
                    body = {...req.body, password};
                }
                await User.findByIdAndUpdate(req.params.id, body, {new: true}, (err, user) => {
                    if (err) {
                        return res.status(400).json({message: "Wrong data to update"});
                    }
                    return res.status(200).json(user);
                });
            } else {
                if (authData.userId === req.params.id) {
                    let body = {...req.body};
                    if (req.body.password) {
                        const password = await bcrypt.hash(req.body.password, 12);
                        body = {...req.body, password};
                    }
                    await User.findByIdAndUpdate(req.params.id, body, {new: true}, (err, user) => {
                        if (err) {
                            return res.status(400).json({message: "Wrong data to update"});
                        }
                        return res.status(200).json({
                            firstName: user.firstName,
                            lastName: user.lastName,
                            id: user._id
                        });
                    });
                }
            }
        });
    } catch (e) {
        res.status(500).json({message: "Something happened"});
    }
};

exports.removeUserById = async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id, (err, user) => {
            if (err) {
                return res.status(400).json({message: "Something happened!"});
            }
            res.status(200).json(user);
        })
    } catch (e) {
        res.status(500).json({message: "Something happened!"});
    }
};
