const mongoose = require('mongoose');
const TaskGroup = require('../db/models/practice/taskGroup.model');
const User = require('../db/models/user.model');

exports.getTaskGroups = async (req, res) => {
    try {
        const taskGroups = await TaskGroup.find({teacher: req.user.userId});
        if (!taskGroups) {
            return res.status(400).json({message: "You don`t have any TaskGroups"});
        }
        res.status(200).json(taskGroups);
    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend"});
    }
};

exports.getTaskGroupByStudentId = async (req, res) => {
    try {
        const users = await User.find({_id: req.params.id});
        const user = users[0];
        const taskGroups = await TaskGroup.find({groups: user.groups[0], active: true});
        if (!taskGroups) {
            return res.status(400).json({message: "You don`t have any TaskGroups"});
        }
        res.status(200).json(taskGroups);
    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend!"});
    }
};

exports.createTaskGroup = async (req, res) => {
    try {
        const teacher = req.user.userId;
        const {groups, title, description, active, expDate, taskType, difficulty, time} = req.body;
        const newTaskGroup = new TaskGroup({
            _id: new mongoose.Types.ObjectId,
            taskType,
            teacher,
            groups,
            title,
            expDate,
            description,
            difficulty,
            time,
            active,
            created: Date.now(),
            updated: Date.now()
        });
        await newTaskGroup.save((err, taskGroup) => {
            if (err) {
                return res.status(400).json({message: "Saving error!"});
            }
            res.status(201).json(taskGroup);
        });

    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend!"});
    }
};

exports.editTaskGroup = async (req, res) => {
    try {
        await TaskGroup.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, taskGroup) => {
            if (err) {
                return res.status(400).json({message: "Wrong updating data"});
            }
            res.status(200).json(taskGroup);
        });
    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend!"})
    }
};

exports.removeTaskGroups = async (req, res) => {
    try {
        await TaskGroup.findByIdAndRemove(req.params.id).exec((err, taskGroup) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(taskGroup);
        });
    } catch (e) {
        console.error(e);
        res.status(500).json({message: "Something happend!"});
    }
};


