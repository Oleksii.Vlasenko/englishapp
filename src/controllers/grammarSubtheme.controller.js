const mongoose = require('mongoose');
const GrammarSubtheme = require('../db/models/practice/grammarSubtheme.model');

exports.getGrammarSubthemes = async (req, res) => {
    try {
        await GrammarSubtheme.find({}).exec((err, themes) => {
            if (err) {
                res.status(400).json({message: "Something happend!"});
            }
            res.status(200).json(themes);
        });
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.createGrammarSubtheme = async (req, res) => {
    try {
        const user = req.user.userId;
        const {title, translation, theme} = req.body;
        const subtheme = await GrammarSubtheme.findOne({title});
        if (subtheme) {
            return res.status(400).json({message: "Subtheme have already registered!"});
        }
        const newGrammarSubtheme = new GrammarSubtheme({
            _id: new mongoose.Types.ObjectId,
            title,
            translation,
            theme,
            user,
            created: Date.now(),
            updated: Date.now()
        });
        await newGrammarSubtheme.save((err, theme) => {
            if (err) {
                return res.status(500).json({message: "Something happened. Try again!"});
            }
            res.status(201).json({message: 'Group successfully saved'});
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.editGrammarSubtheme = async (req, res) => {
    try {
        await GrammarSubtheme.findByIdAndUpdate(req.params.id, req.body, (err) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json({message: "Update successfully!"});
        });

    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeGrammarSubtheme = async (req, res) => {
    try {
        await GrammarSubtheme.findByIdAndRemove(req.params.id).exec((err) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json({message: "Delete successfully!"});
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Error!'});
    }
};
