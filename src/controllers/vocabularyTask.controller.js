const mongoose = require('mongoose');
const VocabularyTask = require('../db/models/practice/vocabularyTask.model');

exports.getVocabularyTasks = async (req, res) => {
    try {
        let regexp = new RegExp(`^` + req.headers.searchstring, `gi`);
        await VocabularyTask.find({word: regexp}).exec((err, tasks) => {
            if (err) {
                return res.status(400).json({message: "Something happend!"});
            }
            if (req.headers['getlist']) {

                const index = req.headers['getlist'];
                tasks.sort((a, b) => {
                    if (a.word < b.word) {
                        return -1;
                    }
                    if (a.word > b.word) {
                        return 1;
                    }
                    return 0;
                });
                const tasksCount = 20;
                const start = index - tasksCount;
                tasks = tasks.filter(item => !item.taskGroups.includes(`${req.headers['taskgroupid']}`));
                return res.status(200).json(tasks.slice(start, index));
            }
            res.status(200).json(tasks);
        });
    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.getVocabularyTaskBySubtheme = async (req, res) => {
    try {
        await VocabularyTask.find({subtheme: req.params.id}).exec(async (err, tasks) => {
            if (tasks.length > 0) {
                return res.status(200).json(tasks);
            }
            await VocabularyTask.find({taskGroups: req.params.id}).exec((err, tasks) => {
                if (tasks) {
                    return res.status(200).json(tasks);
                }
            });
        });

    } catch (e) {
        res.status(500).json({message: "Something happened. Try again!"});
    }
};

exports.createVocabularyTask = async (req, res) => {
    try {
        const user = req.user.userId;
        const {word, transcription, translation, description, taskGroups} = req.body;
        const newVocabularyTask = new VocabularyTask({
            _id: new mongoose.Types.ObjectId,
            word,
            transcription,
            translation,
            description,
            taskGroups,
            user,
            created: Date.now(),
            updated: Date.now()
        });
        await newVocabularyTask.save((err, task) => {
            if (err) {
                return res.status(500).json({message: "Something happened. Try again!"});
            }
            res.status(201).json(task);
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({...e, message: "Something happened. Try again!"});
    }
};

exports.editVocabularyTask = async (req, res) => {
    try {
        await VocabularyTask.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, task) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(task);
        });

    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeVocabularyTask = async (req, res) => {
    try {
        await VocabularyTask.findByIdAndRemove(req.params.id).exec((err, task) => {
            if (err) {
                return res.status(400).json({message: "DB Error"});
            }
            res.status(200).json(task);
        });
    } catch (e) {
        console.error("Error --> ", e.message);
        res.status(500).json({message: 'Error!'});
    }
};
