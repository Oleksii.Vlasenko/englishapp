const mongoose = require('mongoose');
const VocabularyTheme = require('../db/models/practice/vocabularyTheme.model');

exports.getVocabularyThemes = async (req, res) => {
    try {
        await VocabularyTheme.find({}).exec((err, theme) => {
            if (err) {
                return res.status(400).json({message: "Something happend!"})
            }
            res.status(200).json([...theme]);
        })
    } catch (e) {
        res.status(500).json({message: "Something happend!"})
    }
};

exports.createVocabularyTheme = async (req, res) => {
    try {
        const user = req.user.userId;
        const {title, translation} = req.body;
        const theme = await VocabularyTheme.findOne({title});
        if (theme) {
            return res.status(400).json({message: "Theme already in use!"})
        }

        const newTheme = new VocabularyTheme({
            _id: new mongoose.Types.ObjectId,
            title,
            translation,
            user,
            created: Date.now(),
            updated: Date.now()
        });
        await newTheme.save((err) => {
            if (err) {
                return res.status(400).json({message: "Saving error!"});
            }
            res.status(200).json({message: 'Theme successfully saved!'});
        });
    } catch (e) {
        res.status(500).json({message: 'Something happened!'});
    }
};

exports.editVocabularyTheme = async (req, res) => {
    try {
        await VocabularyTheme.findOneAndUpdate({_id: req.params.id}, req.body);
        res.status(200).json({message: "Update successfully!"});
    } catch (e) {
        res.status(500).json({message: 'Something happened. Try again!'});
    }
};

exports.removeVocabularyTheme = async (req, res) => {
    try {
        await VocabularyTheme.findByIdAndRemove(req.params.id).exec((err, theme) => {
            res.json(theme);
        });
    } catch (e) {
        res.status(500).json({message: 'Error!'});
    }
};
