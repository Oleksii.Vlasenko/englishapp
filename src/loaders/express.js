const routes = require('../api/index');
const bodyParser = require('body-parser');

module.exports = ({app}) => {
    app.use(bodyParser.json());
    console.log('expressLoader - Ok');
    app.use('/api', routes());
};
