const expressLoader = require('./express');
const mongodbLoader = require('./mongodb');

module.exports = async ({expressApp}) => {
    await expressLoader({app: expressApp});
    console.log('Express was loaded!');

    await mongodbLoader();
    console.log('MongoDB was loaded');
};
