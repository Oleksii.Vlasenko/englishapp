const mongoose = require('mongoose');
const config = require('config');

const URI = config.get('mongoUri');

module.exports = async () => {
    await mongoose
        .connect(URI,
            {useNewUrlParser: true, useUnifiedTopology: true});
    mongoose.set('useFindAndModify', false);
    return mongoose.connection.db;
};
